
# funciones utiles --------------------------------------------------------------------------------

def limpiar_pantalla
    puts `clear`
end

# Clase Persona -----------------------------------------------------------------------------------
class Persona
    attr_accessor :nombre, :apellido, :rut, :edad, :sexo, :ciudad, :voto

    @@votos = ["A","R","N","B"]
    @@sexo = ["M","F"]

    def initialize(nombre, apellido, rut, edad, sexo, ciudad, voto)
        @nombre = nombre
        @apellido = apellido
        @rut = rut
        @edad = edad
        @sexo = sexo
        @ciudad = ciudad
        @voto = voto
    end

    def imprimir
        puts "---------------------------------------------------------"
        puts "#{self.nombre} #{self.apellido} // Rut : #{self.rut}"
        puts "Edad : #{self.edad} / Sexo : #{self.sexo} / Ciudad : #{self.ciudad}"
        puts "Voto : #{self.voto}"
    end

    def self.validar_voto(entrada)
        @@votos.include?(entrada) ? entrada : false
    end

    def self.validar_sexo(entrada)
        @@sexo.include?(entrada) ? entrada : false
    end
end

# Funciones / Opciones del programa ---------------------------------------------------------------

# opción 1 : agregar persona y voto.
def agregar(arr_p)

    puts "Agregar Persona y Votación al registro. "
    puts "---------------------------------------"
    print "Ingrese Nombre/s : "
    nombre = gets.strip.upcase!
    print "Ingrese Apellidos : "
    apellido = gets.strip.upcase!
    print "Ingrese Rut : "
    rut = gets.strip
    print "Ingrese Edad : "
    edad = gets.strip

    continuar = true
    while continuar
        print "Ingrese Sexo ( F : femenino / M : Masculino ) : "
        sexo = gets.strip
        sexo = sexo.upcase

        if Persona.validar_sexo(sexo)
            continuar = false 
        else
            puts "Entrada no valida ! . Intente nuevamente"
        end
    end

    print "Ingrese Ciudad : "
    ciudad = gets.strip.upcase!

    continuar = true
    while continuar
        print "Ingrese Voto (A : apruebo, R : rechazo, N : nulo, B : blanco) : "
        voto = gets.strip
        voto = voto.upcase

        if Persona.validar_voto(voto)
            continuar = false 
        else
            puts "Entrada no valida ! . Intente nuevamente"
        end
    end

    arr_p << Persona.new(nombre,apellido,rut,edad,sexo,ciudad,voto)

    puts "Se ha añadido de forma exitosa."
end

# 2 : Listar personas y votación.
def listar(arr_p)
    if arr_p.size == 0
        puts "No se han agregado personas al registro."
    else
        for persona in arr_p do
            persona.imprimir
        end
    end
end

# Setup -------------------------------------------------------------------------------------------

array_personas = []

# MENU --------------------------------------------------------------------------------------------

continuar = true
while continuar

    limpiar_pantalla

    puts "M E N U /// sistema de votación 2020 // #{array_personas.length} personas registradas"
    puts ""
    puts "1 : Agregar persona y voto."
    puts "2 : Listar personas y votación."
    puts "3 : Salir."
    puts ""

    print "Ingrese su opción : "

    entrada = gets.strip

    case entrada
    when '1'
        limpiar_pantalla
        agregar(array_personas)
        gets
    when '2'
        limpiar_pantalla
        listar(array_personas)
        gets
    when '3'
        continuar = false
    else
        limpiar_pantalla
        puts "Error, su opción ingresada no es valida."
        gets
    end

end

limpiar_pantalla

puts "Gracias por haber usado nuestro sistema de votación."
puts ""





